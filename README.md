# IN3230/4230 Plenary session code examples #

This repository will contain various code examples shown or related to
the plenary sessions in the
[IN3230/IN4230](https://www.uio.no/studier/emner/matnat/ifi/IN3230/h20/)
Networking course at the University of Oslo, as taught in the fall of
2020.

## Sessions ##

### Plenary 1: 27.08.2020 ###

Examples can be found in the [01_basic_sockets](01_basic_sockets/) directory.

  * Socket API basics
  * Some basic C repetition
  * A really simple client/server connection-oriented example
  * We did not have time to look at raw sockets this week, Kristjon
    covered this in the group session

### Plenary 2: 03.09.2020 ###

Examples are in the [02_raw_sockets](02_raw_sockets/) directory.

 * AF_PACKET raw sockets on Linux
 * A little bit of C memory management

### Plenary 3: 10.09.2020 ###

Examples in the [03_multiplexing](03_multiplexing/) directory.

* I/O multiplexing with epoll (and others)

