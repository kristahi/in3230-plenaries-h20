#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/ethernet.h>	/* ETH_* */

void DumpHex(const void* data, size_t size);

int main(int argc, char *argv[])
{
  int raw_socket;
  uint16_t protocol;


  protocol = htons(ETH_P_ALL);
  raw_socket = socket(AF_PACKET, SOCK_RAW, protocol);
  if (raw_socket == -1) {
    perror("socket");
    return 1;
  }

  int rc;
  uint8_t buf[1600];
  
  do {
    rc = recv(raw_socket, buf, sizeof(buf), 0);

    if (rc == -1) {
      perror("recv");
      return 1;
    }

    DumpHex(buf, rc);
  } while (1);
  
  return 0;
}
