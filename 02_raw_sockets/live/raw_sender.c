#include <stdio.h> 		/* printf */
#include <stdlib.h>
#include <sys/socket.h>		/* socket */
#include <linux/if_packet.h>	/* AF_PACKET */
#include <net/ethernet.h>	/* ETH_* */
#include <arpa/inet.h>		/* htons */
#include <string.h>
#include <sys/types.h>
#include <ifaddrs.h>

#define BUF_SIZE 256
#define ETH_BROADCAST_ADDR {0xff, 0xff, 0xff, 0xff, 0xff, 0xff}

struct ether_frame {
    uint8_t dst_addr[6];
    uint8_t src_addr[6];
    uint8_t eth_proto[2];
    uint8_t contents[0];
} __attribute__((packed));

/* 
 Returns MAC addr as string buffer. Transfers ownership to caller.
 */
char *macaddr_str(struct sockaddr_ll *sa) {
  /* 6*2 hex chars, 5 colon delimiters + NULL: */
  char *macaddr = calloc(6 * 2 + 6, sizeof(char));
  int i;

  for (i = 0; i < 6; i++) {
    char *buf = strdup(macaddr);
    sprintf(macaddr, "%s%02hhx%s",
	    buf,
	    sa->sll_addr[i],
	    (i < 5) ? ":" : "");
    free(buf);
  }

  return macaddr;
}



int main(int argc, char *argv[])
{
  int so, rc;
  struct sockaddr_ll so_name;
  struct ifaddrs *ifaces, *ifp;

  so = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
  if (so == -1) {
    perror("socket");
    return 1;
  }

  /* We get ownership of *ifaces */
  if (getifaddrs(&ifaces)) {
    perror("getifaddrs");
    return 1;
  }

  
  printf("Interface list:\n");
  for (ifp = ifaces; ifp != NULL; ifp = ifp->ifa_next) {
    if (ifp->ifa_addr != NULL && ifp->ifa_addr->sa_family == AF_PACKET) {
      memcpy(&so_name, (struct sockaddr_ll*)ifp->ifa_addr, sizeof(struct sockaddr_ll));
      char *addr_str = macaddr_str(&so_name);

      printf("%s\t%s\n",
	     ifp->ifa_name != NULL ? ifp->ifa_name : "null",
	     addr_str);
      free(addr_str);
    }
  }

  freeifaddrs(ifaces);

  struct ether_frame frame_hdr;
  uint8_t bcast[6] = ETH_BROADCAST_ADDR;

  memcpy(frame_hdr.dst_addr, bcast, 6);
  memcpy(frame_hdr.src_addr, &so_name.sll_addr, 6);
  frame_hdr.eth_proto[0] = frame_hdr.eth_proto[1] = 0xff;

  uint8_t buf[BUF_SIZE];
  strcpy(buf, argv[1]);

  struct msghdr msg;
  struct iovec iov[2];
  memset(&msg, 0, sizeof(struct msghdr));

  iov[0].iov_base = &frame_hdr;
  iov[0].iov_len = sizeof(struct ether_frame);
  iov[1].iov_base = buf;
  iov[1].iov_len = strlen(buf) + 1;

  msg.msg_iov = iov;
  msg.msg_iovlen = 2;

  struct sockaddr_ll dest;
  msg.msg_name = &so_name;
  msg.msg_namelen = sizeof(struct sockaddr_ll);

  rc = sendmsg(so, &msg, 0);
  if (rc == -1) {
    perror("sendmsg");
    return 1;
  }
  
  return 0;
}
