# Second plenary session #

## socket API: raw sockets ##

### Receiving raw frames ###

We started by implementing a super simple network sniffer to read raw
Ethernet frames. It just reads frames in an infinite loop and prints a
hex dump of each frame.

Source code of the sniffer we wrote live during the session:
[live/sniff.c](live/sniff.c)
See [packet_socket.c](packet_socket.c) for an annotated, cleaner version of
the same example.


### Sending raw frames ###

What's the point of being able to read without being able to send?
Next, we looked at how to send frames. This also involves enumerating
the available network interfaces and we used `sendmsg` which is a
slightly more advanced sending call.

Live source code: [live/raw_sender.c](live/raw_sender.c)
Annotated version: [send_packet.c](send_packet.c)


### Running the examples ###

We have provided a makefile to compile the examples, so just run
`make` in either this directory or the `live` subdirectory.

Raw Ethernet frame reading (sniffing) example:
`sudo ./packet_socket`
`sudo ./live/sniff`

Raw Ethernet frame sending example:
`sudo ./send_packet`
`sudo ./live/raw_sender <message>`

Notice that we run the programs as root, since using raw sockets
requires special privileges.
