# First plenary session #

## Basic socket API ##

We implemented a very simple client/server program using UNIX domain
sockets.

Source code of the program we wrote live during the session:
[live/hello_socket.c](live/hello_socket.c)
See [socket_hello.c](socket_hello.c) for an annotated, cleaner version of
the same example.

We didn't have time to look at this during the plenary, but with a few
simple changes, we can modify it to use TCP instead: [tcp_hellod.c](tcp_hellod.c)

### Running the examples ###

We have provided makefiles to compile the examples, so just run
`make` in either this directory or the `live` subdirectory.

The UNIX socket example is run as follows:
server: `./hellod socket_name` (quit with CTRL+C)
client: `./helloc socket_name message`

The TCP example is run like this:
server: `./tcp_hellod portnumber`
client: `./tcp_helloc portnumber message`
