#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>

void server(int so_l, struct sockaddr_un *so_name, char *path);
void client(int so, struct sockaddr_un *so_name, char *msg);

int main(int argc, char *argv[])
{
  char *path;
  int so;

  so = socket(PF_UNIX, SOCK_STREAM, 0);
  if (so == -1) {
    printf("Socket failed: %d", errno);
    return 1;
  }

  path = argv[1];

  struct sockaddr_un so_name;
  so_name.sun_family = PF_UNIX;
  strncpy(so_name.sun_path, path, sizeof(so_name.sun_path) - 1);

  if (strstr(argv[0], "hellod") != NULL) {
    server(so, &so_name, path);
  } else {
    client(so, &so_name, argv[2]);
  }

  return 0;
}

void server(int so_l, struct sockaddr_un *so_name, char *path)
{
  /* SERVER STUFF */
  int rc;
  int so;
  
  printf("I am functioning as a server\n");

  unlink(path);

  /* correcting the "cont" typo to "const" did the trick */
  rc = bind(so_l, (const struct sockaddr*)so_name, sizeof(struct sockaddr_un));
  if (rc == -1) {
    perror("bind");
    return;
  }

  rc = listen(so_l, 5);
  if (rc == -1) {
    perror("listen");
    return;
  }

  for (;;) {
    so = accept(so_l, NULL, NULL);
    if (rc == -1) {
      perror("accept");
      return;
    }

    char buf[256];
    memset(buf, 0, 256);
    
    rc = read(so, buf, 255);
    if (rc == -1) {
      perror("read");
      return;
    }

    printf("%s\n", buf);

    close(so);
  }
}

void client(int so, struct sockaddr_un *so_name, char *msg)
{
  /* CLIENT STUFF */
  int rc;

  /* correcting the "cont" typo to "const" did the trick */
  rc = connect(so, (const struct sockaddr*)so_name, sizeof(struct sockaddr_un));
  if (rc == -1)
    perror("connect");

  rc = write(so, msg, strlen(msg) + 1);
  if (rc == -1)
    perror("write");
}
