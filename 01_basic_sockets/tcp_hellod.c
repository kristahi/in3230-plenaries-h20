#include <stdlib.h> 		/* malloc */
#include <stdio.h>		/* printf */
#include <string.h>		/* memset, strstr, strncpy */
#include <sys/socket.h>		/* socket, bind, listen, accept */
#include <netdb.h>		/* getaddrinfo, struct addrinfo */
#include <unistd.h>		/* read, close, unlink */

#define BUF_SIZE 256

void server(int l_so, struct addrinfo *addr);
void client(int so, struct addrinfo *addr, char *msg);

int main(int argc, char *argv[])
{
  int so, rc;
  struct sockaddr_in so_name;
  struct addrinfo hints, *res;
  
  /* Create socket */
  so = socket(AF_INET, SOCK_STREAM, 0);
  if (so == -1) {
    perror("socket");
    return 1;
  }

  /* The major difference from the UNIX socket example is in how we
     configure the binding/connecting address. */

  /* Zero out name lookup hint struct */
  memset(&hints, 0, sizeof(struct addrinfo));

  /* Resolve IP address info */
  hints.ai_family = AF_INET; 	/* IPv4 */
  hints.ai_socktype = SOCK_STREAM; /* TCP */
  hints.ai_flags = AI_PASSIVE;

  /* We use getaddrinfo to do a dynamic lookup of interface addresses
   *  available. By changing the hints we pass to it, it is for example
   *  possible to automagically try IPv6 if available.
   *  Note that it is entirely possible to manually fill in the
   *  sockaddr_in struct as well.
   */

  /* Specify the port number as the first CLI argument */
  rc = getaddrinfo(NULL, argv[1], &hints, &res);
  if (rc) {
    perror("getaddrinfo");
    return 1;
  }

  if (strstr(argv[0], "hellod") != NULL) {
    server(so, res);
  } else
    client(so, res, argv[2]);

  return 0;
}

void server(int l_so, struct addrinfo *addr)
{
  int rc;
  int so;

  /* Bind socket to socket address (in this case, IP address(es)) */
  rc = bind(l_so, addr->ai_addr, addr->ai_addrlen);
  if (rc == -1) {
    perror("bind");
    return;
  }
  
  /* Listen for connections */
  rc = listen(l_so, 5);
  if (rc == -1) {
    perror("listen");
    return;
  }

  /* Wait for a connection... */
  for (;;) {
    so = accept(l_so, NULL, NULL);
    if (so == -1) {
      perror("accept");
      return; 			/* How about break instead? */
    }

    char *buf = malloc(BUF_SIZE);
    memset(buf, 0, 256);

    do {
      rc = read(so, buf, BUF_SIZE - 1); /* Leaving at least a single
					   null byte at the end of the
					   buffer */

      if (rc > 0) {
	printf("%s\n", buf);
      } else if (rc == 0) {
	printf("Client went away\n");
      } else
	perror("read");

    } while (rc > 0);

    close(so);
  }

  close(l_so);
}

void client(int so, struct addrinfo *addr, char *msg)
{
  int rc;

  rc = connect(so, addr->ai_addr, addr->ai_addrlen);
  if (rc == -1) {
    perror("connect");
    return;
  }

  /* Can we be sure that we are sending the entire msg buffer here? */
  rc = write(so, msg, strlen(msg));
  if (rc == -1) {
    perror("write");
    close(so);
    return;
  }

  close(so);
}
